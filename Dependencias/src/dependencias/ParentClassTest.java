package dependencias;

import org.testng.annotations.Test;

// Inherited dependency test (parent class)
public class ParentClassTest {
	
	// This method depends on the other one
	// and is executed after it
	@Test(dependsOnMethods = {"testTwo"})
    public void testOne() {
        System.out.println("Test method one");
    }
	
	// This method is executed before 'testOne()'
    @Test
    public void testTwo() {
        System.out.println("Test method two");
    }
}
