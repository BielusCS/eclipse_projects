package dependencias;

import org.testng.annotations.Test;

// Writing test with multiple tests dependencies (same class)
public class DependentTestExamples_B {
	
	// This method depends on the others
	// and is executed after them
	@Test(dependsOnMethods = {"testTwo", "testThree"})
    public void testOne() {
        System.out.println("Test method one");
    }
	
	// This method is executed before 'testOne()'
    @Test
    public void testTwo() {
        System.out.println("Test method two");
    }
    
 // This method is executed before 'testOne()'
    @Test
    public void testThree() {
        System.out.println("Test method three");
    }
}
