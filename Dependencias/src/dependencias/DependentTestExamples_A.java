package dependencias;

import org.testng.annotations.Test;

// Writing test with single test dependency (same class)
public class DependentTestExamples_A {
	
	// This method depends on the other one
	// and is executed after it
	@Test(dependsOnMethods = {"testTwo"})
    public void testOne() {
        System.out.println("Test method one");
    }

	// This method is executed before 'testOne()'
    @Test
    public void testTwo() {
        System.out.println("Test method two");
    }
}
