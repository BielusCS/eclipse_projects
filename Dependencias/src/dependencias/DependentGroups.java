package dependencias;

import org.testng.annotations.Test;

// Tests depend on groups (same class)
public class DependentGroups {
	
	// This method depends on the group 'test-group'
	// and is executed after the methods belonging to it
	@Test(dependsOnGroups = {"test-group"})
    public void groupTestOne() {
        System.out.println("Group Test method one");
    }
	
	// This method is executed before 'groupTestOne()'
    @Test(groups = {"test-group"})
    public void groupTestTwo() {
        System.out.println("Group Test method two");
    }
    
    // This method is executed before 'groupTestOne()'
    @Test(groups = {"test-group"})
    public void groupTestThree() {
        System.out.println("Group Test method three");
    }
}
