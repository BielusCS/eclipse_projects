package dependencias;

import org.testng.annotations.Test;

// Inherited dependency test (child class)
public class ChildClassTest extends ParentClassTest {
	
	// This method is executed last of all
	// It depends on 'testOne()' which in
	// turn depends on 'testTwo()'
	@Test(dependsOnMethods = {"testOne"})
    public void testThree() {
        System.out.println("Test three method in Inherited test");
    }
	
	// This method is executed first of all
    @Test
    public void testFour() {
        System.out.println("Test four method in Inherited test");
    }
}
