/**
 * 
 */
package operaciones;

/**
 * @author usuari
 *
 */
public class MultiplicaDivide {
	
	public void multiplica(int a, int b) {
		int multiplica = a * b;
		System.out.println("Multiplicación --> " + multiplica);
	}
	
	public void divide(int a, int b) {
		int divide = a / b;
		System.out.println("División --> " + divide);
	}
}
