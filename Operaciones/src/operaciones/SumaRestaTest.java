package operaciones;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SumaRestaTest {
	
	private SumaResta sr;
	
	/* This method will run before the execution of the first method of the current class */
	@BeforeClass
	public void beforeClass() {
		// Instance which contains all the logic needed by tests
		this.sr = new SumaResta();
	}
	
	/* Parameters are taken from file 'testng.xml'
	 * a --> 10
	 * b --> 2
	 */

	// Test #2 - Subtract
	@Test(priority = -5)
	@Parameters({"a","b"}) 
	public void restaTest(int a, int b) {
		this.sr.resta(a, b);
	}

	// Test #1 - Add
	@Test(priority = -10)
	@Parameters({"a","b"}) 
	public void sumaTest(int a, int b) {
		this.sr.suma(a, b);
	}
}
