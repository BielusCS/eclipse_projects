/**
 * 
 */
package operaciones;

/**
 * @author usuari
 *
 */
public class SumaResta {
	
	public void suma(int a, int b) {
		int suma = a + b;
		System.out.println("Suma --> " + suma);
	}

	public void resta(int a, int b) {
		int resta = a - b;
		System.out.println("Resta --> " + resta);
	}
}
