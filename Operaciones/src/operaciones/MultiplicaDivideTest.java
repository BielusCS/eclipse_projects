package operaciones;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MultiplicaDivideTest {
	
	private MultiplicaDivide md;
	
	/* This method will run before the execution of the first method of the current class */
	@BeforeClass
	public void beforeClass() {
		// Instance which contains all the logic needed by the tests
		this.md = new MultiplicaDivide();
	}
	
	/* Parameters are taken from file 'testng.xml'
	 * a --> 10
	 * b --> 2
	 */

	// Test #4 - Divide
	@Test(priority = 5)
	@Parameters({"a","b"}) 
	public void divideTest(int a, int b) {
		this.md.divide(a, b);
	}

	// Test #3 - Multiply
	@Test(priority = 0)
	@Parameters({"a","b"}) 
	public void multiplicaTest(int a, int b) {
		this.md.multiplica(a, b);
	}
}
