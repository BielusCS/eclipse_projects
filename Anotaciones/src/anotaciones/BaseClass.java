package anotaciones;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseClass {
	
	/* This method will run before the execution of each test method */
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("BaseClass's Before Test method");
	}
	
	/* This method will run after the execution of each test method */
	@AfterMethod
	public void afterMethod() {
		System.out.println("BaseClass's After Test method");
	}
	
	/* This method will run before the execution of the first method of the current class */
	@BeforeClass
	public void beforeClass() {
		System.out.println("BaseClass's Before Class method");
	}
	
	/* This method will run after the execution of all the test methods of the current class */
	@AfterClass
	public void afterClass() {
		System.out.println("BaseClass's After Class method");
	}
}
