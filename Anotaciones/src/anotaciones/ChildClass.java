package anotaciones;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ChildClass extends BaseClass {
	
	/* The program will run without any main() method */
	@Test
	public void cc() {
		System.out.println("===== Executing actual test ======");
	}
	
	/* This method will run before the execution of each test method */
	@BeforeMethod
	public void beforeChildMethod() {
		System.out.println("ChildClass's Before Test method");
	}
	
	/* This method will run after the execution of each test method */
	@AfterMethod
	public void afterChildMethod() {
		System.out.println("ChildClass's After Test method");
	}
	
	/* This method will run before the execution of the first method of the current class */
	@BeforeClass
	public void beforeChildClass() {
		System.out.println("ChildClass's Before Class method");
	}
	
	/* This method will run after the execution of all the test methods of the current class */
	@AfterClass
	public void afterChildClass() {
		System.out.println("ChildClass's After Class method");
	}
}
