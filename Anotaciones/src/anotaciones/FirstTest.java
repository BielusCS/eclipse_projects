package anotaciones;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FirstTest {
	
	/* The program will run without any main() method */
	@Test
	public void ft() {
	}
	
	/* This method will run before the execution of all the test methods in the suite */
	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Before Suite method");
	}
	
	/* This method will run after the execution of all the test methods in the suite */
	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite method");
	}
	
	/* This method will run before the execution of all the test methods of available classes belonging to that folder */
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test method");
	}
	
	/* This method will run after the execution of all the test methods of available classes belonging to that folder */
	@AfterTest
	public void afterTest() {
		System.out.println("After Test method");
	}
	
	/* This method will run before the execution of the first method of the current class */
	@BeforeClass
	public void beforeClass() {
		System.out.println("Before Class method");
	}
	
	/* This method will run after the execution of all the test methods of the current class */
	@AfterClass
	public void afterClass() {
		System.out.println("After Class method");
	}
	
	/* This method will run before the execution of each test method */
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Before Method");
	}
	
	/* This method will run after the execution of each test method */
	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method");
	}
}
