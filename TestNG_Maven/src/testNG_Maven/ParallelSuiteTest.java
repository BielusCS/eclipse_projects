package testNG_Maven;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

// TestNG xml file --> suite-test-testng.xml
public class ParallelSuiteTest {

	private String testName = "";
	
	// This method will be run twice, once for each test defined in TestNG xml file
    @BeforeTest
    @Parameters({"test-name"}) // Its values are: "Test One", "Test Two"
    public void beforeTest(String testName) {
        this.testName = testName;
        long id = Thread.currentThread().getId();
        System.out.println("Before test " + this.testName + ". Thread id is: " + id);
    }

    // This method will be run twice, once for each test defined in TestNG xml file
    @BeforeClass
    public void beforeClass() {
        long id = Thread.currentThread().getId();
        System.out.println("Before test-class " + this.testName + ". Thread id is: " + id);
    }

    // This method will be run twice, once for each test defined in TestNG xml file
    @Test
    public void testMethodOne() {
        long id = Thread.currentThread().getId();
        System.out.println("Sample test-method " + this.testName + ". Thread id is: " + id);
    }

    // This method will be run twice, once for each test defined in TestNG xml file
    @AfterClass
    public void afterClass() {
        long id = Thread.currentThread().getId();
        System.out.println("After test-class " + this.testName + ". Thread id is: " + id);
    }

    // This method will be run twice, once for each test defined in TestNG xml file
    @AfterTest
    public void afterTest() {
        long id = Thread.currentThread().getId();
        System.out.println("After test " + this.testName + ". Thread id is: " + id);
    }
}
