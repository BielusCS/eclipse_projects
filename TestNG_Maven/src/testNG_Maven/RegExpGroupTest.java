package testNG_Maven;

import org.testng.annotations.Test;

// TestNG xml file --> testng.xml
public class RegExpGroupTest {

	/* This method will be invoked
	 * MetaGroup --> "include-group"
	 */
	@Test(groups = {"include-test-one"})
    public void testMethodOne() {
        System.out.println("Test method one");
    }

	/* This method will be invoked
	 * MetaGroup --> "include-group"
	 */
    @Test(groups = {"include-test-two"})
    public void testMethodTwo() {
        System.out.println("Test method two");
    }

    /* This method won't be invoked
     * MetaGroup --> "exclude-group"
     */
    @Test(groups = {"test-one-exclude"})
    public void testMethodThree() {
        System.out.println("Test method three");
    }

    /*
     * This method won't be invoked
     * MetaGroup --> "exclude-group"
     */
    @Test(groups = {"test-two-exclude"})
    public void testMethodFour() {
        System.out.println("Test method four");
    }
}
