package timeout;

import org.testng.annotations.Test;

// Test Timeout at Test Level
public class TimeoutMethodTest {

	// This method takes 1000ms to execute completely
	// It isn't executed because it takes more time to complete than timeout duration defined in annotation
	@Test(timeOut = 500)
    public void timeTestOne() throws InterruptedException {
        Thread.sleep(1000);
        System.out.println("Time test method one");
    }
 
	// This method takes 400ms to execute completely
	// It is executed because it takes less time to complete than timeout duration defined in annotation
    @Test(timeOut = 500)
    public void timeTestTwo() throws InterruptedException {
        Thread.sleep(400);
        System.out.println("Time test method two");
    }
}
