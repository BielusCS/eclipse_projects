package timeout;

import org.testng.annotations.Test;

// Test Timeout at Suite Level
public class TimeoutSuiteTest {

	// This method will take 1000ms to execute completely
	// It isn't executed because it takes more time to complete than timeout duration defined in testng.xml file
	@Test
    public void timeTestOne() throws InterruptedException {
		Thread.sleep(1000);
		System.out.println("Time test method one");
	}

	// This method will take 400ms to execute completely
	// It is executed because it takes less time to complete than timeout duration defined in testng.xml file
	@Test
	public void timeTestTwo() throws InterruptedException {
		Thread.sleep(400);
		System.out.println("Time test method two");
	}
}
