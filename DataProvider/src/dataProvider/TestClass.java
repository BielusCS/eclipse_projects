package dataProvider;

import org.testng.annotations.Test;

/* @DataProvider and @Test in separate classes */
public class TestClass {
	
	/*
	 * dataProvider --> data-set to use
	 * dataProviderClass --> class which contains the data-set
	 */
	@Test(dataProvider = "data-provider", dataProviderClass = DataProviderClass.class)
    public void testMethod(String data) {
        System.out.println("Data is: " + data);
    }
}
