package dataProvider;

import org.testng.annotations.DataProvider;

/* @DataProvider and @Test in separate classes */
public class DataProviderClass {
	
	/* name --> assigned name to data-set */
	@DataProvider(name = "data-provider")
    public static Object[][] dataProviderMethod() {
        return new Object[][] {{"data one"}, {"data two"}};
    }
}
