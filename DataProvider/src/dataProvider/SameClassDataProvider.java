package dataProvider;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/* @DataProvider and @Test in same class */
public class SameClassDataProvider {
	
	/* @DataProvider allows same test method to run multiple times with different data-sets */
	@DataProvider(name = "data-provider") // name --> assigned name to data-set
    public Object[][] dataProviderMethod() {
        return new Object[][] {{"data one"}, {"data two"}};
    }
	
	/* This test method takes one argument as input and prints it to console when executed */
	@Test(dataProvider = "data-provider") // dataProvider --> data-set to use
    public void testMethod(String data) {
        System.out.println("Data is: " + data);
    }
	
	
	/*
	 * @DataProvider helps in providing complex parameters to the test methods as it is not possible to do this from XML.
	 * It is mandatory for a @DataProvider method to return the data in the form of double array of Object class (Object [][]).
	 * The first array represents a data set where as the second array contains the parameter values.
	 */
}
