package enParalelo;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

// Parallel execution of suites
// TestNG xml file --> suite-test-testng.xml
public class ParallelSuiteTest {

	private String testName = "";

	// This method will run before the execution of all the test methods of available classes belonging to that folder
    @BeforeTest
    @Parameters({"test-name"})
    public void beforeTest(String testName) {
        this.testName = testName;
        long id = Thread.currentThread().getId();
        System.out.println("Before test " + this.testName + ". Thread id is: " + id);
    }

    // This method will run before the execution of the first method of the current class
    @BeforeClass
    public void beforeClass() {
        long id = Thread.currentThread().getId();
        System.out.println("Before test-class " + this.testName + ". Thread id is: " + id);
    }

    // This method is a test method
    @Test
    public void testMethodOne() {
        long id = Thread.currentThread().getId();
        System.out.println("Sample test-method " + this.testName + ". Thread id is: " + id);
    }

    // This method will run after the execution of all the test methods of the current class
    @AfterClass
    public void afterClass() {
        long id = Thread.currentThread().getId();
        System.out.println("After test-class " + this.testName + ". Thread id is: " + id);
    }

    // This method will run after the execution of all the test methods of available classes belonging to that folder
    @AfterTest
    public void afterTest() {
        long id = Thread.currentThread().getId();
        System.out.println("After test " + this.testName + ". Thread id is: " + id);
    }
}
