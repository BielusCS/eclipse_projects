package enParalelo;

import org.testng.annotations.Test;

// Test configuration to run in multiple threads
// TestNG xml file --> independent-test-testng.xml
public class IndependentTest {

	// threadPoolSize: number of different threads to run the test
	// invocationCount: number of times the test to be run
	// timeOut: maximum execution duration time
	@Test(threadPoolSize = 3, invocationCount = 6, timeOut = 1000)
    public void testMethod() 
    {
        Long id = Thread.currentThread().getId();
        System.out.println("Test method executing on thread with id: " + id);
    }
}
