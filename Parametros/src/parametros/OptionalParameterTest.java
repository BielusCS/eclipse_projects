package parametros;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

// Optional parameters
public class OptionalParameterTest {

	// This method takes one parameter as input
	// An optional value is defined using the @Optional annotation
	// This method is executed twice: with an optional value and with
	// another passed from testng.xml
	@Parameters({ "optional-value" })
    @Test
    public void optionTest(@Optional("optional value") String value) {
        System.out.println("This is: " + value);
    }
}
