package parametros;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

// Test parameters with testng.xml
public class ParameterTest {

	// This method takes one parameter defined at suite level
	@Parameters({ "suite-param" })
    @Test
    public void parameterTestOne(String param) {
        System.out.println("Test one suite param is: " + param);
    }
	
	// This method takes one parameter defined at test level
	@Parameters({ "test-two-param" })
    @Test
    public void parameterTestTwo(String param) {
        System.out.println("Test two param is: " + param);
    }
	
	// This method takes two parameters. The test parameter is defined
	// at test level. The suite level parameter is overridden at the test level
	@Parameters({ "suite-param", "test-three-param" })
    @Test
    public void parameterTestThree(String param, String paramTwo) {
        System.out.println("Test three suite param is: " + param);
        System.out.println("Test three param is: " + paramTwo);
    }
}

