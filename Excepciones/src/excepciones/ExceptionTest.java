package excepciones;

import java.io.IOException;

import org.testng.annotations.Test;

public class ExceptionTest {
	
	/* If the exception thrown by the test is not part of the user entered list of exceptions, the test will be marked as failed */
	@Test(expectedExceptions = { IOException.class })
    public void exceptionTestOne() throws Exception {
        throw new IOException();
    }
	
	@Test(expectedExceptions = { IOException.class, NullPointerException.class })
    public void exceptionTestTwo() throws Exception {
        throw new Exception();
    }
}
