package excepciones;

import java.io.IOException;

import org.testng.annotations.Test;

public class CustomExceptionTest {
	
	/* 
	 * Regular expressions can be used to verify the error message.
	 * expectedExceptions --> list of exceptions
	 * expectedExceptionsMessageRegExp --> the message must match the specified regular expression
	 */
	@Test(expectedExceptions = { IOException.class }, expectedExceptionsMessageRegExp = "Pass Message test")
	public void exceptionTestOne() throws Exception {
		throw new IOException("Pass Message test");
	}
	 
	@Test(expectedExceptions = { IOException.class }, expectedExceptionsMessageRegExp = ".* Message .*")
	public void exceptionTestTwo() throws Exception {
		throw new IOException("Pass Message test");
	}
	 
	@Test(expectedExceptions = { IOException.class }, expectedExceptionsMessageRegExp = "Pass Message test")
	public void exceptionTestThree() throws Exception {
		throw new IOException("Fail Message test");
	}
}
