package groupExamples;

import org.testng.annotations.Test;

// Test Groups - Including and excluding groups from suite
public class ExcludeGroupTest {

	// This method belongs to group
	@Test(groups = {"include-group"})
    public void testMethodOne() {
        System.out.println("Test method one belonging to include group.");
    }

	// This method belongs to group
    @Test(groups = {"include-group"})
    public void testMethodTwo() {
        System.out.println("Test method two belonging to include group.");
    }

    // This method belongs to both groups
    @Test(groups = {"include-group", "exclude-group"})
    public void testMethodThree() {
        System.out.println("Test method three belonging to include/exclude groups.");
    }
}
