package groupExamples;

import org.testng.annotations.Test;

// Test Groups - Default groups
@Test(groups = {"default-group"})
public class DefaultGroup {

	// This method belongs to 'default-group'
	public void testMethodOne() {
		System.out.println("Test method one.");
	}

	// This method belongs to 'default-group'
	public void testMethodTwo() {
		System.out.println("Test method two.");
	}

	// This method belongs to 'test-group'
	@Test(groups = {"test-group"})
	public void testMethodThree() {
		System.out.println("Test method three.");
	}
}
