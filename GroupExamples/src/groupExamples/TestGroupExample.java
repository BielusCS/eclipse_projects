package groupExamples;

import org.testng.annotations.Test;

// Test Groups - Creating test groups
public class TestGroupExample {

	// This method belongs to group
	@Test(groups = {"test-group"})
	public void testMethodOne() {
		System.out.println("Test method one belonging to group.");
	}

	// This test method doesn't belong to group
	@Test
	public void testMethodTwo() {
		System.out.println("Test method two not belonging to group.");
	}

	// This method belongs to group
	@Test(groups = {"test-group"})
	public void testMethodThree() {
		System.out.println("Test method three belonging to group.");
	}
}
