package groupExamples;

import org.testng.annotations.Test;

// Test Groups - Using regular expressions
public class RegExpGroupTest {

	// This method belongs to an include group
	@Test(groups = {"include-test-one"})
    public void testMethodOne() {
        System.out.println("Test method one");
    }

	// This method belongs to an include group
    @Test(groups = {"include-test-two"})
    public void testMethodTwo() {
        System.out.println("Test method two");
    }

    // This method belongs to an exclude group
    @Test(groups = {"test-one-exclude"})
    public void testMethodThree() {
        System.out.println("Test method three");
    }

    // This method belongs to an exclude group
    @Test(groups = {"test-two-exclude"})
    public void testMethodFour() {
        System.out.println("Test method four");
    }
}
