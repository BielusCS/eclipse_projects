package groupExamples;

import org.testng.annotations.Test;

// Test Groups - Writing tests to multiple groups
public class MultiGroupExample {

	// This method belongs to group 'group-one'
	@Test(groups = {"group-one"})
    public void testMethodOne() {
        System.out.println("Test method one belonging to group.");
    }

	// This method belongs to both groups
	@Test(groups = {"group-one", "group-two"})
    public void testMethodTwo() {
        System.out.println("Test method two belonging to both groups.");
    }

	// This method belongs to group 'group-two'
	@Test(groups = {"group-two"})
    public void testMethodThree() {
        System.out.println("Test method three belonging to group.");
    }
}
