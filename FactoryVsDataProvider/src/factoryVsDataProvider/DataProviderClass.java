package factoryVsDataProvider;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

// Factory vs DataProvider - Using DataProvider
public class DataProviderClass {

	// This method will run before the execution of the first method of the current class
	@BeforeClass
    public void beforeClass() {
        System.out.println("Before class executed");
    }

	// This method takes the data-set from 'dataMethod()'
	// It is invoked twice, once for each instance of the class
    @Test(dataProvider = "dataMethod")
    public void testMethod(String param) {
        System.out.println("The parameter value is: " + param);
    }

    // Data-set consisting of 2 values
    // The constructor is invoked twice
    @DataProvider
    public Object[][] dataMethod() {
        return new Object[][] {{"one"}, {"two"}};
    }
}
