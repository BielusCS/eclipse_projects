package factoryVsDataProvider;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

// Factory vs DataProvider - Using Factory (main class)
public class SimpleTest {

	private String param;

	// The constructor is invoked twice from @Factory
	// The coming value is assigned to local variable 'param'
    public SimpleTest(String param) {
        this.param = param;
    }

    // This method will run before the execution of the first method of the current class
    @BeforeClass
    public void beforeClass() {
        System.out.println("Before SimpleTest class executed.");
    }

	// This method is invoked twice, once for each instance of the class
    @Test
    public void testMethod() {
        System.out.println("testMethod parameter value is: " + param);
    }
}
