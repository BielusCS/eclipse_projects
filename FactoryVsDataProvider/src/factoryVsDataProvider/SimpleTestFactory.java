package factoryVsDataProvider;

import org.testng.annotations.Factory;

// Factory vs DataProvider - Using Factory (specific class)
public class SimpleTestFactory {

	// Passing parameters to test classes while initializing them is possible
	// These parameters can be used across all the test methods present in the initialized classes
	@Factory
    public Object[] factoryMethod() {
        return new Object[] { 
            new SimpleTest("one"), 
            new SimpleTest("two") 
        };
	}
}
