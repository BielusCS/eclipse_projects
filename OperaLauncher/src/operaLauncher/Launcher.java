/**
 * 
 */
package operaLauncher;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.opera.OperaDriver;

/**
 * @author usuari
 *
 */
public class Launcher {
	// Interface which represents the web browser
	private WebDriver wd;
	// Interface which represents a web element
    private WebElement we;
    
    // This method establishes a connection with the web browser
    public void connectToOpera() {
    	// Web driver and its path are indicated
        System.setProperty("webdriver.opera.driver", "/home/usuari/NetBeansProjects/operadriver");
        // Instance of the driver to communicate with the web browser
        this.wd = new OperaDriver();
    }
    
    // This method navigates to Google
    public void navigateToGoogle() {
        this.wd.navigate().to("http://www.google.com");
    }
    
    // This method finds a web element and assigns it to 'we'
    public void findElement(String id) {
        this.we = this.wd.findElement(By.name(id));
    }
    
    // This method sends a string to a web element (e.g. a search bar)
    public void sendKeys(String keys) {
    	this.we.sendKeys(keys);
    }
    
    // This method finds the search button and clicks it
    public void clickBtn() throws InterruptedException {
        Thread.sleep(3000);
        this.we = this.wd.findElement(By.name("btnK"));
        this.we.click();
    }
    
    // This method finishes the connection with the web browser
    public void closeDriver() throws InterruptedException {
        Thread.sleep(3000);
        this.wd.close();
    }
}
