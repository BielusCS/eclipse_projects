package operaLauncher;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

public class LauncherTest {

	private Launcher l;
	
	// This method will run before the execution of the first method of the current class
	@BeforeClass
	public void beforeClass() {
		// Instance which contains all the logic needed by the tests
		this.l = new Launcher();
	}
	
	// Test #5 - Click the button to search for the matching results
	@Test(priority = 5)
	public void clickBtnTest() {
		try {
			this.l.clickBtn();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// Test #6 - Finish the connection with the web browser
	@Test(priority = 10)
	public void closeDriverTest() {
		try {
			this.l.closeDriver();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// Test #1 - Connect to and open the web browser
	@Test(priority = -15)
	public void connectToOperaTest() {
		this.l.connectToOpera();
	}
	
	// Test #3 - Find the search bar
	@Test(priority = -5)
	public void findElementTest() {
		this.l.findElement("q");
	}
	
	// Test #2 - Navigate to a specified web site
	@Test(priority = -10)
	public void navigateToGoogleTest() {
		this.l.navigateToGoogle();
	}

	// Test #4 - Write the desired string into the search bar
	@Test(priority = 0)
	public void sendKeysTest() {
		this.l.sendKeys("Tutorials Point");
	}
}
