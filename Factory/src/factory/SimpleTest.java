package factory;

import org.testng.annotations.Test;

// Factory parameters - Main class
public class SimpleTest {

	private int param;

	// The constructor is invoked twice from @Factory
	// The coming value is assigned to local variable 'param'
    public SimpleTest(int param) {
        this.param = param;
    }
    
    // This method is invoked twice, once for each instance of the class
    @Test
    public void testMethodOne() {
        int opValue = param + 1;
        System.out.println("Test method one output: " + opValue);
    }

    // This method is invoked twice, once for each instance of the class
    @Test
    public void testMethodTwo() {
        int opValue = param + 2;
        System.out.println("Test method two output: " + opValue);
    }
}
