package factory;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

// Factory and DataProvider - Create tests at runtime
public class DataProviderTest {

	private int param;

	// This constructor takes the data-set from 'dataMethod()'
	// The coming value is assigned to local variable 'param'
    @Factory(dataProvider = "dataMethod")
    public DataProviderTest(int param) {
        this.param = param;
    }

    // Data-set consisting of 2 values
    // The constructor is invoked twice
    @DataProvider
    public static Object[][] dataMethod() {
        return new Object[][] {{0}, {1}};
    }

    // This method is invoked twice, once for each instance of the class
    @Test
    public void testMethodOne() {
        int opValue = param + 1;
        System.out.println("Test method one output: " + opValue);
    }

    // This method is invoked twice, once for each instance of the class
    @Test
    public void testMethodTwo() {
        int opValue = param + 2;
        System.out.println("Test method two output: " + opValue);
    }
}
