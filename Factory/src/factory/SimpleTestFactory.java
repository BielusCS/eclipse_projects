package factory;

import org.testng.annotations.Factory;

// Factory parameters - Factory class
public class SimpleTestFactory {

	// Passing parameters to test classes while initializing them is possible.
	// These parameters can be used across all the test methods present in the initialized classes.
	@Factory
    public Object[] factoryMethod() {
        return new Object[] {
        		new SimpleTest(0),
        		new SimpleTest(1)
        };
    }
}
