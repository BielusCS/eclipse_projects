package factory;

import org.testng.annotations.Test;

// Factory - Dependent tests
public class DependencyTest {

	private int param;

	// The constructor is invoked twice from @Factory
	// The coming value is assigned to local variable 'param'
    public DependencyTest(int param) {
        this.param = param;
    }

    // This method is invoked twice, once for each instance of the class
    @Test(dependsOnMethods = {"testMethodTwo"})
    public void testMethodOne() {
        System.out.println("Test method one with param values: " + this.param);
    }

    // This method is invoked twice, once for each instance of the class
    // This method is executed before 'testMethodOne()'
    @Test
    public void testMethodTwo() {
        System.out.println("Test method two with param values: " + this.param);
    }
}
