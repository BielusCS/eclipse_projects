package factory;

import org.testng.annotations.Factory;

// Factory dependent tests - Factory class
public class SimpleTestFactory_Dependency {

	// Passing parameters to test classes while initializing them is possible.
	// These parameters can be used across all the test methods present in the initialized classes.
	@Factory
    public Object[] factoryMethod() {
        return new Object[] {
        		new DependencyTest(1),
        		new DependencyTest(2)
        };
    }
}
